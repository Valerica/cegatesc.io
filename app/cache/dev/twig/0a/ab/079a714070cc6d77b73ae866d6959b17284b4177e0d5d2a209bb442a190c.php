<?php

/* ::base.html.twig */
class __TwigTemplate_0aab079a714070cc6d77b73ae866d6959b17284b4177e0d5d2a209bb442a190c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    
    <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    
    <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    
    <!--[if IE]>
    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/ie.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <![endif]-->  
        
    ";
        // line 20
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "    
    <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.js"), "html", null, true);
        echo "\"></script>
    
    ";
        // line 24
        $this->displayBlock('javascripts', $context, $blocks);
        // line 25
        echo "    
</head>

<body>
    
<div id=\"layout-content\">
    
    ";
        // line 32
        $this->env->loadTemplate("::header.html.twig")->display($context);
        echo "   
    
    <div class=\"container\">
    
    ";
        // line 36
        $this->displayBlock('content', $context, $blocks);
        // line 37
        echo "        
    </div> 

</div> <!-- layout-content END -->

    <footer>

        footer content

    </footer>

</body>
</html>
";
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        echo "Nume site";
    }

    // line 20
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
    }

    // line 36
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 36,  114 => 24,  109 => 20,  103 => 10,  86 => 37,  84 => 36,  77 => 32,  68 => 25,  66 => 24,  61 => 22,  58 => 21,  56 => 20,  50 => 17,  44 => 14,  39 => 12,  34 => 10,  23 => 1,);
    }
}
