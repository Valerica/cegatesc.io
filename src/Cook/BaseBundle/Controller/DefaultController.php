<?php

namespace Cook\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CookBaseBundle:Default:index.html.twig');
    }
}
